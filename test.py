from math import pi

def circumference(r):
    return 2*pi*r

def area(r):
    """
    This function calculates the area of a circle, with radius r
    """
    return pi*r*r
